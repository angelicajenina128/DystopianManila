﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Transform keyFollowPoint;

    public Key followingKey;
    float speed = 20f;
    Rigidbody rb;
    bool stateIsGrounded = false;
    bool stateIsLaddered = false;
    Vector3 movement;

    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        movement = new Vector3(Input.GetAxisRaw("Horizontal") * speed, rb.velocity.y, 0f);

        if (Input.GetKeyDown(KeyCode.W) && stateIsGrounded && !stateIsLaddered)
        {
            rb.AddForce(new Vector3(0, 40f, 0), ForceMode.Impulse);
            stateIsGrounded = false;


        }

        if (Input.GetKeyDown(KeyCode.W) && stateIsLaddered)
        {
            rb.velocity = new Vector3(rb.velocity.x, speed, rb.velocity.z);
        }


        if (Input.GetKeyDown(KeyCode.S) && stateIsLaddered)
        {
            rb.velocity = new Vector3(rb.velocity.x, -speed, rb.velocity.z);
        }

    }
    void FixedUpdate()
    {
        moveCharacter(movement);
    }

    void moveCharacter(Vector3 direction)
    {
        rb.velocity = direction;

        if (!stateIsLaddered)
            rb.AddForce(new Vector3(0, -2f, 0), ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
            stateIsGrounded = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ladder")
            stateIsLaddered = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Ladder")
            stateIsLaddered = false;
    }
}